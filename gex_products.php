<?php
  header('Content-Type: application/json; Charset=ISO-8859-1');
  include('ws_settings.php');

  $storeID = "";
  $categoryID = "";


  if(isset($_REQUEST['category_id']) && isset($_REQUEST['store_id']) ){

    $storeID = $_REQUEST['store_id'];
    $categoryID = $_REQUEST['category_id'];

    $productQuery = "SELECT * FROM h7jml_virtuemart_product_categories AS product_categories INNER JOIN h7jml_virtuemart_products_es_es AS products ON product_categories.virtuemart_product_id = products.virtuemart_product_id INNER JOIN h7jml_virtuemart_product_prices ON h7jml_virtuemart_product_prices.virtuemart_product_id = products.virtuemart_product_id WHERE product_categories.virtuemart_category_id = ".$categoryID.";";
    $productResult = mysql_query($productQuery);

    while($productRow = mysql_fetch_assoc($productResult)){

      $productId = $productRow['virtuemart_product_id'];
      $product_s_desc = $productRow['product_s_desc'];
      $product_desc = $productRow['product_desc'];
      $product_name = $productRow['product_name'];
      $customtitle = $productRow['customtitle'];
      $slug = $productRow['slug'];
      $categoryId = $productRow['virtuemart_category_id'];
      $productPrice = $productRow['product_price'];

      //$productCategoryQuery = "SELECT * FROM h7jml_virtuemart_product_categories WHERE ";
      $objectProductDB[] = array('product_id'=>htmlentities(strip_tags($productId)),'product_s_description'=>htmlentities(strip_tags($product_s_desc)),'product_description'=>htmlentities(strip_tags($product_desc)),'product_name'=>$product_name,'product_price'=>number_format($productPrice,2),'product_customtitle'=>$customtitle,'product_slug'=>$slug,'category_id'=>$categoryId);
    }
    $objectProductJSON = json_encode($objectProductDB);
    echo '{"products":'.$objectProductJSON.'}';

  }else{
    echo '{"products": "not record found (Look your store_id and category_id)"}';
  }

?>
